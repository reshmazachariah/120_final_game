﻿using System;
using UnityEngine;

/// <summary>
/// A painting that can be carried or dropped.
/// </summary>
public interface IPainting
{
    bool CanPickUp { get; }
    void PickUp(Transform carryLocation);
    void Drop();
}
