﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaintingTrigger : MonoBehaviour
{
    /// <summary>
    /// The painting for this trigger.
    /// </summary>
    public IPainting Painting { get; set; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (Painting == null)
            return;

        // If the object colliding with us wants to know about it, then it
        // has a PaintingTriggerReceiver script.
        var receiver = collision.GetComponent<PaintingTriggerReceiver>();

        if (receiver != null)
            receiver.OnEnterPainting(Painting);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (Painting == null)
            return;

        // If the object colliding with us wants to know about it, then it
        // has a PaintingTriggerReceiver script.
        var receiver = collision.GetComponent<PaintingTriggerReceiver>();

        if (receiver != null)
            receiver.OnExitPainting(Painting);
    }
}
