﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class PlayerScript : MonoBehaviour
{
    public BasePlayerState State => currentState;
    public Transform paintingPosition;

    private void Awake()
    {
        currentState = BasePlayerState.GetInitialState(this);
    }

    private void Update()
    {
        currentState.Update();
    }

    private BasePlayerState currentState;

    /// <summary>
    /// The base class for the player's state. Player states should extend
    /// this class and implement the <see cref="Update"/> method.
    /// </summary>
    public abstract class BasePlayerState
    {
        public static BasePlayerState GetInitialState(PlayerScript player)
        {
            return new PlayerStates.WalkingState(new InitialState(player));
        }

        /// <summary>
        /// Called once every frame.
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Called when this state is transitioned to, before the first <see cref="Update"/>.
        /// </summary>
        protected virtual void Initialize() { }

        /// <summary>
        /// Called when this state is transitioned out of.
        /// </summary>
        protected virtual void Cleanup() { }

        protected BasePlayerState(BasePlayerState previous)
        {
            Player = previous.Player;
        }

        protected void TransitionTo(BasePlayerState nextState)
        {
            Cleanup();
            Player.currentState = nextState;
            nextState.Initialize();
        }

        protected PlayerScript Player { get; private set; }

        /// <summary>
        /// Private constructor to be used only by <see cref="InitialState"/>.
        /// </summary>
        /// <param name="player">Player.</param>
        private BasePlayerState(PlayerScript player)
        {
            Player = player;
        }

        private class InitialState : BasePlayerState
        {
            public InitialState(PlayerScript p)
                : base(p)
            {
            }

            public override void Update()
            {
                throw new System.NotSupportedException();
            }
        }
    }
}
