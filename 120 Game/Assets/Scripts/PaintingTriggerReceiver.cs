﻿using System;
using System.Linq;
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This component is added to anything that cares about interacting with paintings.
/// </summary>
public class PaintingTriggerReceiver : MonoBehaviour
{
    /// <summary>
    /// Is this object in front of a painting?
    /// </summary>
    public bool IsInFrontOfPainting => TopMovablePainting != null;

    public IPainting TopMovablePainting => paintings.FirstOrDefault(painting => painting.CanPickUp);

    public void OnEnterPainting(IPainting painting)
    {
        paintings.AddFirst(painting);
    }

    public void OnExitPainting(IPainting painting)
    {
        paintings.Remove(painting);
    }

    private readonly LinkedList<IPainting> paintings = new LinkedList<IPainting>();
}
