﻿using System;
using UnityEngine;

namespace PlayerStates
{
    /// <summary>
    /// This is the default state for the player during which the player can
    /// move normally and isn't interacting with a painting.
    /// </summary>
    public class WalkingState : PlayerScript.BasePlayerState
    {
        public WalkingState(PlayerScript.BasePlayerState previous)
            : base(previous)
        {
            animator = Player.GetComponent<Animator>();
            transform = Player.GetComponent<Transform>();
            paintingDetector = Player.GetComponent<PaintingTriggerReceiver>();
        }

        public override void Update()
        {
            float move = Input.GetAxis("Horizontal");

            animator.SetFloat("MovementSpeed", move);

            transform.Translate(move / 10, 0, 0);

            if (Input.GetKeyDown(KeyCode.UpArrow) && paintingDetector.IsInFrontOfPainting)
                TransitionTo(new JumpingState(this, paintingDetector.TopMovablePainting));
        }

        private readonly Transform transform;
        private readonly Animator animator;
        private readonly PaintingTriggerReceiver paintingDetector;
    }
}