﻿using System;
using UnityEngine;

namespace PlayerStates
{
    public class SneakingState : PlayerScript.BasePlayerState
    {
        public Transform paintingPosition;

        public SneakingState(PlayerScript.BasePlayerState previous, IPainting paintingToCarry)
            : base(previous)
        {
            animator = Player.GetComponent<Animator>();
            transform = Player.GetComponent<Transform>();
            carriedPainting = paintingToCarry;
            Debug.Log("The player is sneaking around!");
        }

        protected override void Initialize()
        {
            // Carry the painting by attaching it to the player.
            carriedPainting.PickUp(Player.paintingPosition);
        }


        public override void Update()
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                carriedPainting.Drop();
                TransitionTo(new WalkingState(this));
            }

            float move = Input.GetAxis("Horizontal");

            animator.SetFloat("MovementSpeed", move);

            transform.Translate(move / 10, 0, 0);
        }

        private readonly Transform transform;
        private readonly Animator animator;
        private readonly IPainting carriedPainting;
    }
}