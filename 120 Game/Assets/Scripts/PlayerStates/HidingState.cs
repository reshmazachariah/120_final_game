﻿using System;
using UnityEngine;

namespace PlayerStates
{
    public class HidingState : PlayerScript.BasePlayerState
    {
        public HidingState(PlayerScript.BasePlayerState previous, IPainting hidingPainting)
            : base(previous)
        {
            this.hidingPainting = hidingPainting;
            Debug.Log("The player is hiding!");

            animator = Player.GetComponent<Animator>();
        }

        public override void Update()
        {
            if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                animator.SetTrigger("Drop");
                TransitionTo(new WalkingState(this));
            }
            else if (Input.GetKeyDown(KeyCode.RightArrow) ||
                Input.GetKeyDown(KeyCode.LeftArrow))
            {
                animator.SetTrigger("Sneak");
                TransitionTo(new SneakingState(this, hidingPainting));
            }
        }

        private readonly IPainting hidingPainting;
        private readonly Animator animator;
    }

}