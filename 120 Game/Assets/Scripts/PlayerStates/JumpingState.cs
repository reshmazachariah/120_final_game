﻿using System;
using UnityEngine;

namespace PlayerStates
{
    public class JumpingState : PlayerScript.BasePlayerState
    {
        public JumpingState(PlayerScript.BasePlayerState previous, IPainting jumpingIntoPainting)
            : base(previous)
        {
            animator = Player.GetComponent<Animator>();
            this.jumpingIntoPainting = jumpingIntoPainting;
            playerAnimationEvents = Player.GetComponent<PlayerAnimationEvents>();
        }

        protected override void Initialize()
        {
            animator.SetTrigger("Jump");
            playerAnimationEvents.JumpFinished += OnJumpFinished;
        }

        protected override void Cleanup()
        {
            playerAnimationEvents.JumpFinished -= OnJumpFinished;
        }

        public override void Update()
        {

        }

        private void OnJumpFinished()
        {
            TransitionTo(new HidingState(this, jumpingIntoPainting));
        }

        private readonly Animator animator;
        private readonly PlayerAnimationEvents playerAnimationEvents;
        private readonly IPainting jumpingIntoPainting;
    }
}