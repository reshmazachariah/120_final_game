﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCameraOnTrigger : MonoBehaviour
{
    public Transform newCameraPos;
    public Transform cameraPos;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            cameraPos.position = newCameraPos.position;
        }
    }
}
