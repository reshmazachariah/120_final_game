﻿using System;
using UnityEngine;

namespace PaintingStates
{
    public class CarriedState : PaintingScript.BasePaintingState
    {
        public override bool CanPickUp => false;

        public CarriedState(PaintingScript.BasePaintingState previous, Transform carryLocation)
            : base(previous)
        {
            this.carryLocation = carryLocation;
            sprite = Painting.GetComponent<SpriteRenderer>();
        }

        protected override void Initialize()
        {
            originalParent = Painting.transform.parent;
            Painting.transform.parent = carryLocation;

            sprite.sortingLayerName = "Carried";
        }

        protected override void Cleanup()
        {
            Painting.transform.parent = originalParent;

            sprite.sortingLayerName = "Paintings";
        }

        private Transform originalParent;
        private readonly Transform carryLocation;
        private SpriteRenderer sprite;
    }
}