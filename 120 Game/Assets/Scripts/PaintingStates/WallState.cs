﻿using System;
using UnityEngine;

namespace PaintingStates
{
    public class WallState : PaintingScript.BasePaintingState
    {
        public override bool CanPickUp => true;

        public WallState(PaintingScript.BasePaintingState previous)
            : base(previous)
        {
        }
    }
}