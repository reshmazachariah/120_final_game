﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HighScoreScript : MonoBehaviour
{
    public void OnTriggerStay2D(Collider2D collider)
    {
        PlayerScript player = collider.GetComponent<PlayerScript>();

        if (player != null && player.State is PlayerStates.WalkingState || player != null && player.State is PlayerStates.SneakingState)
        {
            SceneManager.LoadScene("Win");
        }


    }
}
