﻿using System;
using UnityEngine;

[RequireComponent(typeof(PaintingTrigger))]
public class PaintingScript : MonoBehaviour, IPainting
{
    public bool CanPickUp => currentState.CanPickUp;

    public void PickUp(Transform carryLocation)
    {
        currentState.PickUp(carryLocation);
    }

    public void Drop()
    {
        currentState.Drop();
    }

    private void Awake()
    {
        paintingTrigger = GetComponent<PaintingTrigger>();
        paintingTrigger.Painting = this;

        currentState = BasePaintingState.GetInitialState(this);
    }

    private void Update()
    {
        currentState.Update();
    }

    private BasePaintingState currentState;
    private PaintingTrigger paintingTrigger;

    /// <summary>
    /// The base class for the painting's state. Painting states should extend
    /// this class and implement the <see cref="Update"/> method.
    /// </summary>
    public abstract class BasePaintingState
    {
        public static BasePaintingState GetInitialState(PaintingScript painting)
        {
            return new PaintingStates.WallState(new InitialState(painting));
        }

        /// <summary>
        /// Can the painting be picked up in this state?
        /// </summary>
        public abstract bool CanPickUp { get; }

        /// <summary>
        /// Picks up the painting. The default implementation transitions to
        /// <see cref="PaintingStates.CarriedState"/>.
        /// </summary>
        public virtual void PickUp(Transform carryLocation)
        {
            TransitionTo(new PaintingStates.CarriedState(this, carryLocation));
        }

        /// <summary>
        /// Drops the painting. The default implementation transitions to
        /// <see cref="PaintingStates.WallState"/>.
        /// </summary>
        public virtual void Drop()
        {
            TransitionTo(new PaintingStates.WallState(this));
        }

        /// <summary>
        /// Called once every frame.
        /// </summary>
        public virtual void Update() { }

        /// <summary>
        /// Called when this state is transitioned to, before the first <see cref="Update"/>.
        /// </summary>
        protected virtual void Initialize() { }

        /// <summary>
        /// Called when this state is transitioned out of.
        /// </summary>
        protected virtual void Cleanup() { }

        protected BasePaintingState(BasePaintingState previous)
        {
            Painting = previous.Painting;
        }

        protected void TransitionTo(BasePaintingState nextState)
        {
            Cleanup();
            Painting.currentState = nextState;
            nextState.Initialize();
        }

        protected PaintingScript Painting { get; private set; }

        /// <summary>
        /// Private constructor to be used only by <see cref="InitialState"/>.
        /// </summary>
        private BasePaintingState(PaintingScript painting)
        {
            Painting = painting;
        }

        private class InitialState : BasePaintingState
        {
            public InitialState(PaintingScript p)
                : base(p)
            {
            }

            public override bool CanPickUp => throw new NotSupportedException();
        }
    }
}
