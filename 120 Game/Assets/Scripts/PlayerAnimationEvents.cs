﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This contains events that should be triggered by the animation system for
/// the player class.
/// </summary>
[RequireComponent(typeof(AudioSource))]
public class PlayerAnimationEvents : MonoBehaviour
{
    [SerializeField]
    private AudioClip footstepSound = default;

    /// <summary>
    /// Occurs when the jump animation reaches the end.
    /// </summary>
    public event Action JumpFinished;

    public void Step()
    {
        source.PlayOneShot(footstepSound);
    }

    public void FinishJump()
    {
        JumpFinished?.Invoke();
    }

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    private AudioSource source;
}
