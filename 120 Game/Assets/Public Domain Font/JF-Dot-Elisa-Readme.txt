JFドット恵梨沙フォント8
Version 1.00.20150527


■ このフォントについて

JFドット恵梨沙フォント8は、「恵梨沙フォント」をアウトライン化された TrueType 形式のフォントに変換し、
Windows や Mac、Linux など幅広い環境で使用できるようにしたフォントのセットです。
フォントにはビットマップも含んでいます。


■ ライセンスについて

元の「恵梨沙フォント」に準じます。
--------------------------
・恵梨沙フォントの著作権の扱いについて
他機種用へコンバートされた恵梨沙フォントを含むすべての
恵梨沙フォントの著作権は、恵梨沙フォントＰＪが保有しています。

・雑誌・書籍等での紹介、収録について
雑誌・書籍等の記事での紹介、付属ディスク類への収録に
際しては事前連絡が必要です。代表者ＹＡＦＯまでご連絡下さい。

・ホームページでの紹介、リンクについて
個人・非営利団体のホームページでの紹介、リンクは自由ですが
ご連絡いただけるとうれしいです。

営利団体（企業、出版社など）のホームページでの紹介、リンクに
際しては事前連絡が必要です。代表者ＹＡＦＯまでご連絡下さい。

・恵梨沙フォントの使用条件について

原則として個人的な用途に限り使用を認めます。
ただし、別途商用利用のご相談にも応じております。
http://hp.vector.co.jp/authors/VA002310/commerce.htm
--------------------------


■ 恵梨沙フォント 原版頒布元

恵梨沙フォントプロジェクト
http://hp.vector.co.jp/authors/VA002310/


■ アウトライン TrueType 版頒布元

自家製フォント工房
http://jikasei.me/font/kh-dotfont/


■ 改版履歴


●Version.1.000.20150527

・Mac OS X の塗りつぶし問題に対処するため、アウトラインの作成方法を根本的に見直した


●Version.1.000.20150526

・Mac OS X で、一部文字の中が黒く塗りつぶされているのを修正


●Version.1.000.20150524

・初回リリース
